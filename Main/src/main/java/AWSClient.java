import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.machinelearning.AmazonMachineLearning;
import com.amazonaws.services.machinelearning.AmazonMachineLearningClientBuilder;
import com.amazonaws.services.machinelearning.model.*;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AWSClient {

    private AmazonMachineLearning client;
    private String friendlyEntityName;
    private String trainDataSourceId;
    private String testDataSourceId;
    private int trainPercent = 70;
    private String trainingDataUrl;
    private String schemaFilename;
    private String mlModelId;
    private String evaluationId;
    private String recipeFilename;
    public static String bucketName= "ttpsc-bartlo";
    public static  String fileObjKeyName= "myFile.csv";

    public AWSClient(String friendlyName, String trainingDataUrl, String schemaFilename, String recipeFilename) {
        this.client = AmazonMachineLearningClientBuilder.defaultClient();
        this.friendlyEntityName = friendlyName;
        this.trainingDataUrl = trainingDataUrl;
        this.schemaFilename = schemaFilename;
        this.recipeFilename = recipeFilename;
    }

    public static void main(String[] args) throws IOException {
        String trainingDataUrl = "s3://".concat(bucketName).concat("/").concat(fileObjKeyName);
        String schemaFilename = "schema.xml";
        String recipeFilename = "recipe.json";
        String friendlyEntityName = "Java Marketing Sample";
        AWSClient builder = new AWSClient(friendlyEntityName, trainingDataUrl, schemaFilename, recipeFilename);
        builder.build();


    }

    private void build() throws IOException {
        uploadFile(bucketName, fileObjKeyName);
        createDataSources();
        createModel();
        createEvaluation();
    }

    private void createDataSources() throws IOException {
        trainDataSourceId = Identifiers.newDataSourceId();
        createDataSource(trainDataSourceId, friendlyEntityName + " - training data", 0, trainPercent);

        testDataSourceId = Identifiers.newDataSourceId();
        createDataSource(testDataSourceId, friendlyEntityName + " - testing data", trainPercent, 100);
    }

    private void createDataSource(String entityId, String entityName, int percentBegin, int percentEnd) throws IOException {
        String dataSchema = Util.loadFile(schemaFilename);
        String dataRearrangementString = "{\"splitting\":{\"percentBegin\":" + percentBegin + ",\"percentEnd\":" + percentEnd + "}}";
        CreateDataSourceFromS3Request request = new CreateDataSourceFromS3Request().withDataSourceId(entityId).withDataSourceName(entityName).withComputeStatistics(true);
        S3DataSpec dataSpec = new S3DataSpec().withDataLocationS3(trainingDataUrl).withDataRearrangement(dataRearrangementString).withDataSchema(dataSchema);
        request.setDataSpec(dataSpec);
        client.createDataSourceFromS3(request);
        System.out.printf("Created DataSource %s with id %s\n", entityName, entityId);
    }

    /**
     * Creates an ML Model object, which begins the training process.
     * The quality of the model that the training algorithm produces depends
     * primarily on the data, but also on the hyper-parameters specified in
     * the parameters map, and the feature-processing recipe.
     *
     * @throws IOException
     */
    private void createModel() throws IOException {
        mlModelId = Identifiers.newMLModelId();

        Map<String, String> parameters = new HashMap<>();
        parameters.put("sgd.maxPasses", "100");
        parameters.put("sgd.maxMLModelSizeInBytes", "104857600");//100MiB
        parameters.put("sgd.l2RegularizationAmount", "1e-4");

        CreateMLModelRequest request = new CreateMLModelRequest().withMLModelId(mlModelId).withMLModelName(friendlyEntityName + "model").withMLModelType(MLModelType.BINARY).withParameters
                (parameters).withRecipe(Util.loadFile(recipeFilename)).withTrainingDataSourceId(trainDataSourceId);
        client.createMLModel(request);
        System.out.printf("CreatedMLModel with id%s\n", mlModelId);
    }

    /**
     * Creates an Evaluation, which measures the quality of the ML Model
     * by seeing how many predictions it gets correct, when run on a
     * held-out sample (30%) of the original data.
     */
    private void createEvaluation() {
        evaluationId = Identifiers.newEvaluationId();
        // evaluationId = "ev-" + UUID.randomUUID().toString();  // simpler, a bit more ugly
        CreateEvaluationRequest request = new CreateEvaluationRequest()
                .withEvaluationDataSourceId(testDataSourceId)
                .withEvaluationName(friendlyEntityName + " evaluation")
                .withMLModelId(mlModelId);
        client.createEvaluation(request);
        System.out.printf("Created Evaluation with id %s\n", evaluationId);
    }

    private void uploadFile(String bucketName, String fileObjKeyName) {
        String clientRegion = "eu-west-1";

        String fileName = "banking.csv";

        try {
            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(clientRegion)
                    .withCredentials(new ProfileCredentialsProvider())
                    .build();

            // Upload a file as a new object with ContentType and title specified.
            PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName, new File(fileName));
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("plain/text");
            metadata.addUserMetadata("x-amz-meta-title", "someTitle");
            request.setMetadata(metadata);
            s3Client.putObject(request);
        }
        catch(AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }


    }

}